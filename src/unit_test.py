'''
Author: Derek Aherhe
Date: 2020-04-24
contact: derek.aherne@sse.com

Description:
A unit test suite for pipeline.py

'''


from pipeline import substr_id_type , map_it , filter_dict , filter_dict_id , dict_constuctor , substr_class , process_gen , process_region , process_region_price , pipeline
import unittest
from io import StringIO
from csv import reader
import pandas as pd
import numpy as np
import logging


class Test_pipes(unittest.TestCase):
    
    #mock up lists
    files = ['ST Generator(x).Generation.csv']
    
    #mock up dicts and lists
    lookup = {'Generator x': 'ABC', 'Region x': 'ABC'}
    
    my_dict = {'KEY':{'CLASS':'CLASS_VALUE','TYPE':'TYPE_VALUE','ID':'ID_VALUE'},
               'KEY2':{'CLASS':'CLASS2_VALUE','TYPE':'TYPE2_VALUE','ID':'ID2_VALUE'}}
    
    filtered_dict = {'KEY':{'CLASS':'CLASS_VALUE','TYPE':'TYPE_VALUE','ID':'ID_VALUE'}}
    
    filtered_value = {'CLASS':'CLASS_VALUE','TYPE':'TYPE_VALUE'}
    
    
    #mock up dictionary
    def mock_dict(self,_class,_type,_id):
        _dict = {'ST {}({}).{}.csv'.format(_class,_id,_type):
                  {
                    'class': _class,
                    'id': _id,
                    'name': 'ABC',
                    'type': _type
                  }}
        return _dict
        
    
    #mock up df
    def mock_df(self):
        cols1 = ['YEAR','MONTH','DAY']
        nrow = 30
        cols2 = [x for x in range(1,25)]
        df = pd.DataFrame(np.random.randint(0,10000,size=(nrow,len(cols2) )), columns=cols2)
        dict_1 = {
            'YEAR':[2020 for x in range(1,nrow+1)],
            'MONTH':[4 for x in range(1,nrow+1)],
            'DAY':[x for x in range(1,nrow+1)]

        }
        return pd.concat([pd.DataFrame(dict_1),df],axis=1)
    
    
    def test_substr(self):
        result = substr_id_type('ST KEY(1).TYPE_VALUE.csv','(',')')
        result_2 = substr_class('ST KEY(1).TYPE_VALUE.csv')
        self.assertEqual(result,'1')
        self.assertEqual(result_2,'KEY')
    
    def test_map_it(self):
        result = map_it(self.lookup,'Generator x')
        self.assertEqual(result,'ABC')
        
    def test_filter_dict(self):      
        result = filter_dict(self.my_dict,'CLASS_VALUE','TYPE_VALUE')
        self.assertDictEqual(result,self.filtered_dict)

    def test_filter_dict_id(self):
        result = filter_dict_id(self.my_dict,'CLASS_VALUE','TYPE_VALUE','ID_VALUE')
        self.assertDictEqual(result,self.filtered_dict)

    def test_dict_constuctor(self):
        result = dict_constuctor(self.files,self.lookup)
        _dict = self.mock_dict('Generator','Generation','x')
        self.assertDictEqual(result,_dict)
        
    def test_process_region(self):
        df_reg = self.mock_df()
        regresult = process_region(df_reg,'NAME')
        self.assertEqual(len(regresult.columns),26)
        self.assertEqual(regresult.columns[0],'SITE')
        self.assertEqual(len(regresult['SITE']),1)

    def test_process_gen(self):
        df_gen = self.mock_df()
        result = process_gen(df_gen,'NAME')
        self.assertEqual(len(result),2)
        self.assertEqual(result['SITE'],'NAME')
                   
    def test_process_region_price(self):
        df_prc = self.mock_df()
        result = process_region_price(df_prc,'NAME')
        self.assertEqual(len(result.columns),48+2)
        self.assertEqual(result.columns[0],'SITE')
        self.assertEqual(len(result['SITE']),30)
        
        
    def test_pipeline_reg(self):
        _class = 'Region'
        _type = 'Generation'
        _id = 'x'
        _dict = self.mock_dict(_class,_type,_id)
        result = pipeline(_dict, _class, _type, _id, self.mock_df())
        self.assertEqual(len(result.columns),26)
        self.assertEqual(result.columns[0],'SITE')
        self.assertEqual(len(result['SITE']),1)

    def test_pipeline_gen(self):
        _class = 'Generator'
        _type = 'Generation'
        _id = 'x'
        _dict = self.mock_dict(_class,_type,_id)
        result = pipeline(_dict, _class, _type, _id, self.mock_df())
        self.assertEqual(len(result.columns),2)
        self.assertEqual(result.columns[0],'SITE')
        self.assertEqual(len(result['SITE']),1)


    def test_pipeline_reg_prc(self):
        _class = 'Region'
        _type = 'Price'
        _id = 'x'
        _dict = self.mock_dict(_class,_type,_id)
        result = pipeline(_dict, _class, _type, _id, self.mock_df())
        self.assertEqual(len(result.columns),50)
        self.assertEqual(result.columns[0],'SITE')
        self.assertEqual(len(result['SITE']),30)
        
if __name__ == '__main__':   
    unittest.main()