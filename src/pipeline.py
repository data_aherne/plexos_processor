'''
Author: Derek Aherhe
Date: 2020-04-24
contact: derek.aherne@sse.com

Description:
A module that defines all the functions for processing a Plexos model output

'''

import os
import csv
import re
import pandas as pd 
import calendar as cal
import logging
logger = logging.getLogger(__name__)

def map_it(lookup,value):
    try:
        a = lookup[value]
    except:
        print('error with {}'.format(value))
    return a


def filter_dict(_adict,_class,filetype):
    '''
    A function to filter a dictionary
    
    param: a dictionary
    param: a filter class
    param: a filter type
    return: a dictionary
    ''' 
    _filter_d = {}
    for item in _adict.items():
        if _class in item[1].values() and filetype in item[1].values():
            _filter_d.update(dict([item]))
    return _filter_d

def filter_dict_id(_adict,_class,filetype,_id):
    '''
    A function to filter a dictionary
    
    param: a dictionary
    param: a filter class
    param: a filter type
    param: a filter id
    return: a dictionary
    ''' 
    _filter_d = {}
    for item in _adict.items():
        it_vals = item[1].values()
        if _class in it_vals and filetype in it_vals and _id in it_vals:
            _filter_d.update(dict([item]))
    return _filter_d

def substr_id_type(s,sp1,sp2):
    '''
    A function to extract the id or file type from a file name 
    
    param: a string 
    param: the first character or substring 
    param: the second character or substring 
    return: a substring
    '''
    start = s.find(sp1)+1 
    end =  s.rfind(sp2)
    return s[start:end]

def substr_class(s):
    '''
    A function to extract the class from a file name
    
    param: a string 
    return: a substring
    ''' 
    end =  s.find('(')
    return s[3:end]

def dict_constuctor(file_list,lookup):
    '''
    A function to build a dictionary of files based on the string vlaues
    
    param: a list of file names
    return: a dictionary
    ''' 
    file_dict = dict.fromkeys(file_list)
    return {k:{'id':substr_id_type(k,'(',')')
               ,'type':substr_id_type(k,'.','.')
               ,'class':substr_class(k) 
               ,'name':map_it(lookup,'{}'.format(substr_class(k)+' '+substr_id_type(k,'(',')')))
                                          }
            for (k,v) in file_dict.items()}


def process_gen(df,name):
    '''
    Process a generator dataframe that is a time series. 
    Aggregate using the mean and reshape to wide
    
    param: a data frame
    param: a site name
    return: a dictionary
    ''' 

    # processes and housekeeping
    
    df['DATE'] = pd.to_datetime(df[["YEAR", "MONTH",'DAY']])
    df.set_index(['DATE',"YEAR", "MONTH"], inplace=True)
    df.drop(columns=['DAY'],inplace=True)
    df['mean'] = df.mean(axis=1)
    df.drop(df.columns[:24],axis=1,inplace=True) #drop now redundant columns
    df.reset_index(inplace=True)

    #SELECT YEAR,MONTH,AVG(mean),MIN(DATE) FROM TABLE GROUP BY YEAR,MONTH
    df = df.groupby(['YEAR','MONTH']).agg(
    {    'mean':'mean',    # MEAN per group
         'DATE': 'first'  # get the first date per group
    })
    df['SITE'] = name #give the df a name
    #reshape
    df = df.pivot(index='SITE', columns='DATE', values='mean').reset_index()
    df.columns = [str(date_obj)[:7]  for date_obj in df.columns] #clean up headers
    return df.to_dict('records')[0]

def process_region(df,name):
    '''
    Process a region dataframe that is a time series. 
    
    param: a data frame
    param: a site name
    return: a data frame
    ''' 
    df.drop(columns=['DAY'],inplace=True)
    #SELECT YEAR,MONTH,AVG(mean) FROM TABLE GROUP BY YEAR,MONTH
    df = df.groupby(['YEAR','MONTH'],as_index=False).mean()
    df.insert (0, "YEARMON", df['MONTH'].apply(lambda x: cal.month_abbr[x])+'-'+df['YEAR'].astype(str))
    df.insert (0,'SITE', name)
    df.drop(columns=['YEAR','MONTH'],inplace=True)
    
    return df

def process_region_price(df,name):
    '''
    Process a region price dataframe that is a time series. 
    
    param: a data frame
    return: a dataframe
    ''' 
    df=pd.melt(df,id_vars=['YEAR','MONTH','DAY'],var_name='HOUR', value_name='VALUES')
    df['TIME'] = pd.to_datetime(df[["YEAR", "MONTH",'DAY','HOUR']], unit='s')  - pd.Timedelta('01:00:00')
    df = df.set_index(['TIME'])
    df = df.resample('30min').pad()
    df = df.reset_index()
    df['HOUR'] = df['TIME'].dt.strftime('%H:%M')
    df['DATE'] = pd.to_datetime(df['TIME'].dt.strftime('%Y-%m-%d'))
    df = pd.concat([df,df.pivot(columns='HOUR', values='VALUES')],axis=1)
    df = df.groupby(['DATE']).sum()
    df.drop(columns = ['YEAR','MONTH','VALUES','DAY'],inplace=True)
    df.reset_index(inplace=True)
    #df['DATE'] = df['DATE'].dt.strftime('%b-%Y')
    df = df.groupby(['DATE'],as_index=False).mean()
    df.insert (0,'SITE', name)
    
    return df

def pipeline(_dict, file_class, file_type, _id = None, df_test = None):
    '''
    Process dictionary with file names for keys 
    
    param: a dictionary
    param: a file class
    param: a file type name
    return: a list of dataframes
    ''' 
    if file_class == 'Region':
        pipe_dict = filter_dict_id(_dict,file_class,file_type,_id)
        logging.info('Regional subset of master dict created with: {}'.format(len(pipe_dict)))
    else:
        pipe_dict = filter_dict(_dict,file_class,file_type)
        logging.info('Generator subset of master dict created with: {}'.format(len(pipe_dict)))
        df_dicts = []
    
    for k,v in pipe_dict.items():
        logging.info('File processed: {}'.format(k))
        try:
            df = pd.read_csv('../data/{}'.format(k),skiprows=[1],header=0)
        except:
            df = df_test

        if file_type == 'Price':
            df = process_region_price(df,v['name'])
        elif file_class == 'Generator':
            df = process_gen(df,v['name'])
            df_dicts.append(df)
        else:
            df = process_region(df,v['name'])

    if file_class == 'Generator':
        return pd.DataFrame(df_dicts)
    else:
        #pass
        return df
    