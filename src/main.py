'''
Author: Derek Aherne
Date: 2020-04-24
contact: derek.aherne@sse.com

Description:
A data processing script to handle the outputs of a PLexos model:
--A list of files is generated
--A lookup dictionary is created
--A master dictionary is created from the above
--pipeline methods are called to generate dfs for:
  --Generator - Generation, Available Capacity and Forced Outage 
  --Region 2 - Exports, Imports, Generation and Load
  --Region 2 - Prices
--The data frames are written to excel

'''

from pipeline import dict_constuctor , pipeline
import os
import csv
import re
import pandas as pd
import logging
import datetime

# setup
logfile = '../log/run_{}.log'.format(datetime.date.today().strftime("%Y%m%d"))
logging.basicConfig(filename=logfile,level=logging.DEBUG,format='%(asctime)s %(levelname)s-%(message)s')

def main():
    data_loc = '../data/'
    output_loc = '../output/'
    log = '../log/'
    files = []
    file_omits = []
    
    # Get files that start with 'ST'
    for f_name in os.listdir(data_loc):
        if f_name.startswith('ST') and f_name.count('(') == 1:            
               files.append(f_name)
        else:
               file_omits.append(f_name)
                
    logging.info('generating list of files to be processed, file count: {}'.format(len(files)))
    logging.info('files omitted count: {}'.format(len(file_omits)))
    for file in file_omits:
        logging.info('file omitted : {}'.format(file_omits))
                    
    # create a lookup dictionary
    with open('{}id2name.csv'.format(data_loc)) as fin:
        csvin = csv.reader(fin)
        lookup = {re.sub(' +', ' ',row[0]+row[1]):row[2].strip() for row in csvin}
        logging.info('lookup dictionary created with {} items'.format(len(lookup)))
                     
    #master dictioanry
    my_dict = dict_constuctor(files,lookup)
    logging.info('master input dictionary created with {} items'.format(len(my_dict)))
      
    #############################################################
    #Generator - Generation, Available Capacity and Forced Outage
    #############################################################
    df_avail_cap = pipeline(my_dict,'Generator','Available Capacity')
    df_gen= pipeline(my_dict,'Generator','Generation')
    df_out= pipeline(my_dict,'Generator','Forced Outage')
    # write to file
    output_name = '{}Generator.xlsx'.format(output_loc)
    with pd.ExcelWriter(output_name) as writer:  
        df_avail_cap.to_excel(writer, sheet_name='Available Capacity',index=False)
        df_gen.to_excel(writer, sheet_name='Generation',index=False)
        df_out.to_excel(writer, sheet_name='Forced Outage',index=False)
    logging.info('Generator Workbook downloaded to {}'.format(output_name)) 
    #################################################
    #Region 2 - Exports, Imports, Generation and Load
    #################################################
    df_reg_exp = pipeline(my_dict,'Region','Exports','2')
    df_reg_imp = pipeline(my_dict,'Region','Imports','2')
    df_reg_gen = pipeline(my_dict,'Region','Generation','2')
    df_reg_load = pipeline(my_dict,'Region','Load','2')
    #write to file
    output_name = '{}Region.xlsx'.format(output_loc)
    with pd.ExcelWriter(output_name) as writer:  
        df_reg_exp.to_excel(writer, sheet_name='Exports',index=False)
        df_reg_imp.to_excel(writer, sheet_name='Imports',index=False)
        df_reg_gen.to_excel(writer, sheet_name='Generation',index=False)
        df_reg_load.to_excel(writer, sheet_name='Load',index=False)
    logging.info('Region Workbook downloaded to {}'.format(output_name)) 
    ##################
    #Region 2 - Prices
    ##################
    df_reg_prc = pipeline(my_dict,'Region','Price','2')
    #write to file
    output_name = '{}Region_Price.xlsx'.format(output_loc)            
    with pd.ExcelWriter(output_name) as writer:  
        df_reg_prc.to_excel(writer, sheet_name='Price',index=False)
    logging.info('Region Price Workbook downloaded to {}'.format(output_name))  
                 
if __name__ == '__main__':
    main()