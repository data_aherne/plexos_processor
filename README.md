----------------------------------------------------------------------------------

## Plexos File Processor

Author: Derek Aherne
Date: 2020-04-24
contact: derek.aherne@sse.com

----------------------------------------------------------------------------------

## Change History

2020-04-24 - Created by Derek Aherne

### Description:

A file processing script to handle the output of a PLexos model. 
The files are aggreagted by mean values and require reshaping in 
one or both directions (both directions when resampling):

The processing steps:

--A list of files is generated
--A lookup dictionary is created
--A master dictionary is created from the previous steps
--pipeline methods are called to generate dataframes for:
  --Generator - Generation, Available Capacity and Forced Outage 
  --Region 2 - Exports, Imports, Generation and Load
  --Region 2 - Prices
--The data frames are written to 3 seperate excel work books

----------------------------------------------------------------------------------

### File Structure
The file and folder struture is below

plexos_prpcessor
    |--- README.md
    |--- data
    |---  src
        |---  pipeline.py
        |---  main.py
        |---  unit_test.py
        |---  ouput
    |--- log

The data folder contains the input files - the output files from a plexos model. 
The src folder contains all the python scripts for running the app and unit testing
The output folder contains the Excel Workbooks writtten to file
The log contains the testing and run time log files

----------------------------------------------------------------------------------

### Script Details

##### main.py

A list of file names is generated from the data folder
Badly named files will be ommitted eg. ST Generator(1).Generation.(1).csv
consult the log file after each run for file omissions.

A dictionary is created using dict_constructor(). The resulting dictionary contains 
all the file names and relevant details about that file

An example of a dictionary item:
{'ST Region(1).Price.csv':
                  {
                    'class': 'Region',
                    'id': '2',
                    'name': 'SEM',
                    'type': 'Price'
                  }
}

The pipeline() function is used to process a dataframe

Example:

pipeline(my_dict,'Generator','Available Capacity') will take a master dictionary as 
its first argument. The pipeline filters the dictionary to a dictionary that only 
has 'Generator' items with a value = 'Available Capacity' . 

The pipline loops through the file names uploading them to memory as dataframes and 
passing them to a processing function which returns a processed dataframe.

The returned data frame is written to file as an Excel Workbook

##### pipeline.py

A module that defines all the functions for processing a Plexos model output 
This module is imported and utilised in main.py

##### unit_test.py

A unit test suite for pipeline.py. 

----------------------------------------------------------------------------------

### Run Book:
A set of appropiate files need to exist in the data folder 
--the output of a Plexos model
--including id2name.csv

to run the app type the following in a PowerShell or Anaconda terminal command line
python .\src\main.py

----------------------------------------------------------------------------------

### Testing:
The script unit_test.py contains a suite of unit tests. Unit testing is not exhaustive 
so if you encounter an error you should replicate this with a new test ;-) 

To run the test suite type the following in a PowerShell command line
python .\src\unit_test.py

----------------------------------------------------------------------------------



